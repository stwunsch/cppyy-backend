diff --git a/src/core/metacling/src/TClingMethodInfo.cxx b/src/core/metacling/src/TClingMethodInfo.cxx
index 0dbf27a298..bbfb6aa962 100644
--- a/src/core/metacling/src/TClingMethodInfo.cxx
+++ b/src/core/metacling/src/TClingMethodInfo.cxx
@@ -82,6 +82,42 @@ private:
    Iterator fEnd;
 };
 
+class TClingMethodInfo::UsingIterator
+{
+public:
+   typedef clang::UsingDecl::shadow_iterator Iterator;
+
+   UsingIterator(cling::Interpreter* interp, Iterator begin, Iterator end) : fInterp(interp), fIter(begin), fEnd(end) {}
+   explicit UsingIterator(cling::Interpreter* interp, clang::UsingDecl *decl) :
+       fInterp(interp), fIter(decl->shadow_begin()), fEnd(decl->shadow_end()) {}
+
+   FunctionDecl *operator* () const {
+      clang::ConstructorUsingShadowDecl* shadow_ctor = llvm::dyn_cast<clang::ConstructorUsingShadowDecl>(*fIter);
+      if (shadow_ctor) {
+          clang::CXXConstructorDecl* base_ctor = llvm::dyn_cast<clang::CXXConstructorDecl>(shadow_ctor->getTargetDecl());
+          if (base_ctor) {
+              if (base_ctor->isImplicit()) return nullptr; // skip as Cling will generate these anyway
+              return fInterp->getSema().findInheritingConstructor(base_ctor->getLocStart(), base_ctor, shadow_ctor);
+          }
+      }
+      return llvm::dyn_cast<clang::FunctionDecl>((*fIter)->getTargetDecl()); }
+   FunctionDecl *operator-> () const { return this->operator*(); }
+   UsingIterator & operator++ () { ++fIter; return *this; }
+   UsingIterator   operator++ (int) {
+      UsingIterator tmp(fInterp, fIter,fEnd);
+      ++(*this);
+      return tmp;
+   }
+   bool operator!() { return fIter == fEnd; }
+   operator bool() { return fIter != fEnd; }
+
+private:
+   cling::Interpreter* fInterp;
+   Iterator fIter;
+   Iterator fEnd;
+};
+
+
 TClingMethodInfo::TClingMethodInfo(const TClingMethodInfo &rhs) :
    fInterp(rhs.fInterp),
    fContexts(rhs.fContexts),
@@ -90,20 +126,27 @@ TClingMethodInfo::TClingMethodInfo(const TClingMethodInfo &rhs) :
    fIter(rhs.fIter),
    fTitle(rhs.fTitle),
    fTemplateSpecIter(nullptr),
-   fSingleDecl(rhs.fSingleDecl)
+   fSingleDecl(rhs.fSingleDecl),
+   fUsingIter(nullptr)
 {
    if (rhs.fTemplateSpecIter) {
       // The SpecIterator query the decl.
       R__LOCKGUARD(gInterpreterMutex);
       fTemplateSpecIter = new SpecIterator(*rhs.fTemplateSpecIter);
    }
+
+   if (rhs.fUsingIter) {
+      // Internal loop over using shadow decls active
+      R__LOCKGUARD(gInterpreterMutex);
+      fUsingIter = new UsingIterator(*rhs.fUsingIter);
+   }
 }
 
 
 TClingMethodInfo::TClingMethodInfo(cling::Interpreter *interp,
                                    TClingClassInfo *ci)
    : fInterp(interp), fFirstTime(true), fContextIdx(0U), fTitle(""),
-     fTemplateSpecIter(0), fSingleDecl(0)
+     fTemplateSpecIter(0), fSingleDecl(0), fUsingIter(0)
 {
    R__LOCKGUARD(gInterpreterMutex);
 
@@ -132,7 +175,7 @@ TClingMethodInfo::TClingMethodInfo(cling::Interpreter *interp,
 TClingMethodInfo::TClingMethodInfo(cling::Interpreter *interp,
                                    const clang::FunctionDecl *FD)
    : fInterp(interp), fFirstTime(true), fContextIdx(0U), fTitle(""),
-     fTemplateSpecIter(0), fSingleDecl(FD)
+     fTemplateSpecIter(0), fSingleDecl(FD), fUsingIter(0)
 {
 
 }
@@ -140,6 +183,7 @@ TClingMethodInfo::TClingMethodInfo(cling::Interpreter *interp,
 TClingMethodInfo::~TClingMethodInfo()
 {
    delete fTemplateSpecIter;
+   delete fUsingIter;
 }
 
 TDictionary::DeclId_t TClingMethodInfo::GetDeclId() const
@@ -162,6 +206,9 @@ const clang::FunctionDecl *TClingMethodInfo::GetMethodDecl() const
    if (fTemplateSpecIter)
       return *(*fTemplateSpecIter);
 
+   if (fUsingIter)
+      return *(*fUsingIter);
+
    return llvm::dyn_cast<clang::FunctionDecl>(*fIter);
 }
 
@@ -204,6 +251,8 @@ void TClingMethodInfo::Init(const clang::FunctionDecl *decl)
    delete fTemplateSpecIter;
    fTemplateSpecIter = 0;
    fSingleDecl = decl;
+   delete fUsingIter;
+   fUsingIter = 0;
 }
 
 void *TClingMethodInfo::InterfaceMethod(const ROOT::TMetaUtils::TNormalizedCtxt &normCtxt) const
@@ -224,6 +273,10 @@ bool TClingMethodInfo::IsValidSlow() const
       R__LOCKGUARD(gInterpreterMutex);
       cling::Interpreter::PushTransactionRAII RAII(fInterp);
       return *(*fTemplateSpecIter);
+   } else if (fUsingIter) {
+      R__LOCKGUARD(gInterpreterMutex);
+      cling::Interpreter::PushTransactionRAII RAII(fInterp);
+      return *(*fUsingIter);
    }
    return *fIter;
 }
@@ -397,6 +450,13 @@ int TClingMethodInfo::InternalNext()
             } else {
                return 1;
             }
+         } else if (fUsingIter) {
+            while (++(*fUsingIter)) {
+               if (*(*fUsingIter))
+                  return 1;
+            }
+            delete fUsingIter; fUsingIter = 0;
+            ++fIter;
          } else {
             ++fIter;
          }
@@ -444,6 +504,17 @@ int TClingMethodInfo::InternalNext()
          }
       }
 
+      clang::UsingDecl* udecl =
+          llvm::dyn_cast<clang::UsingDecl>(*fIter);
+
+      if ( udecl ) {
+          // A UsingDecl potentially brings in a bunch of functions, so
+          // start an inner loop to catch them all
+          delete fUsingIter;
+          fUsingIter = new UsingIterator(fInterp, udecl);
+          return 1;
+      }
+
       // Return if this decl is a function or method.
       if (llvm::isa<clang::FunctionDecl>(*fIter)) {
          // Iterator is now valid.
diff --git a/src/core/metacling/src/TClingMethodInfo.h b/src/core/metacling/src/TClingMethodInfo.h
index 8b7f50bf1c..7e3156def0 100644
--- a/src/core/metacling/src/TClingMethodInfo.h
+++ b/src/core/metacling/src/TClingMethodInfo.h
@@ -52,6 +52,7 @@ class TClingTypeInfo;
 class TClingMethodInfo {
 private:
    class SpecIterator;
+   class UsingIterator;
 
    cling::Interpreter                          *fInterp; // Cling interpreter, we do *not* own.
    llvm::SmallVector<clang::DeclContext *, 2>   fContexts; // Set of DeclContext that we will iterate over.
@@ -61,13 +62,14 @@ private:
    std::string                                  fTitle; // The meta info for the method.
    SpecIterator                                *fTemplateSpecIter; // Iter over template specialization. [We own]
    const clang::FunctionDecl                   *fSingleDecl; // The single member
+   UsingIterator                               *fUsingIter; // for internal loop over using functions
 
    bool IsValidSlow() const;
 
 public:
    explicit TClingMethodInfo(cling::Interpreter *interp)
       : fInterp(interp), fFirstTime(true), fContextIdx(0U), fTitle(""),
-        fTemplateSpecIter(0), fSingleDecl(0) {}
+        fTemplateSpecIter(0), fSingleDecl(0), fUsingIter(0) {}
 
    TClingMethodInfo(const TClingMethodInfo&);
 
